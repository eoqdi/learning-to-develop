//
//  ViewController.h
//  firstCalc
//
//  Created by Emad Oqdi on 15/05/2014.
//  Copyright (c) 2014 EmadOqdi. All rights reserved.
//

#import <UIKit/UIKit.h>


int Method;
double SelectNumber;
double RunningNumber;



@interface ViewController : UIViewController
{
    IBOutlet UILabel *Screen;
}

-(IBAction)Numebr1:(id)sender;
-(IBAction)Numebr2:(id)sender;
-(IBAction)Numebr3:(id)sender;
-(IBAction)Numebr4:(id)sender;
-(IBAction)Numebr5:(id)sender;
-(IBAction)Numebr6:(id)sender;
-(IBAction)Numebr7:(id)sender;
-(IBAction)Numebr8:(id)sender;
-(IBAction)Numebr9:(id)sender;
-(IBAction)Numebr0:(id)sender;
-(IBAction)Times:(id)sender;
-(IBAction)Subtract:(id)sender;
-(IBAction)Devide:(id)sender;
-(IBAction)Plus:(id)sender;
-(IBAction)Equals:(id)sender;
-(IBAction)Clear:(id)sender;




@end
