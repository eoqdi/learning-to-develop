//
//  ViewController.m
//  firstCalc
//
//  Created by Emad Oqdi on 15/05/2014.
//  Copyright (c) 2014 EmadOqdi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

-(IBAction)Numebr1:(id)sender
{
    SelectNumber = SelectNumber * 10;
    SelectNumber = SelectNumber + 1;
    Screen.text = [NSString stringWithFormat:@"%2.f", SelectNumber];
    
    
}

-(IBAction)Numebr2:(id)sender
{
    SelectNumber = SelectNumber * 10;
    SelectNumber = SelectNumber + 2;
    Screen.text = [NSString stringWithFormat:@"%2.f", SelectNumber];
}

-(IBAction)Numebr3:(id)sender
{
    SelectNumber = SelectNumber * 10;
    SelectNumber = SelectNumber + 3;
    Screen.text = [NSString stringWithFormat:@"%2.f", SelectNumber];
    
}

-(IBAction)Numebr4:(id)sender
{
    SelectNumber = SelectNumber * 10;
    SelectNumber = SelectNumber + 4;
    Screen.text = [NSString stringWithFormat:@"%2.f", SelectNumber];
    
}

-(IBAction)Numebr5:(id)sender
{
    SelectNumber = SelectNumber * 10;
    SelectNumber = SelectNumber + 5;
    Screen.text = [NSString stringWithFormat:@"%2.f", SelectNumber];
    
}

-(IBAction)Numebr6:(id)sender
{
    SelectNumber = SelectNumber * 10;
    SelectNumber = SelectNumber + 6;
    Screen.text = [NSString stringWithFormat:@"%2.f", SelectNumber];
    
}

-(IBAction)Numebr7:(id)sender
{
    SelectNumber = SelectNumber * 10;
    SelectNumber = SelectNumber + 7;
    Screen.text = [NSString stringWithFormat:@"%2.f", SelectNumber];
    
}

-(IBAction)Numebr8:(id)sender
{
    SelectNumber = SelectNumber * 10;
    SelectNumber = SelectNumber + 8;
    Screen.text = [NSString stringWithFormat:@"%2.f", SelectNumber];
}

-(IBAction)Numebr9:(id)sender
{
    SelectNumber = SelectNumber * 10;
    SelectNumber = SelectNumber + 9;
    Screen.text = [NSString stringWithFormat:@"%2.f", SelectNumber];
    
}

-(IBAction)Numebr0:(id)sender
{
    SelectNumber = SelectNumber * 10;
    SelectNumber = SelectNumber + 0;
    Screen.text = [NSString stringWithFormat:@"%2.f", SelectNumber];
    
}


-(IBAction)Times:(id)sender
{
    if (RunningNumber == 0) {
        RunningNumber = SelectNumber;
    }
    else{
        
    switch(Method)
        {
            case 1:
    RunningNumber = RunningNumber * SelectNumber;
            break;
                
            case 2:
    RunningNumber = RunningNumber - SelectNumber;
            break;
                
            case 3:
    RunningNumber = RunningNumber / SelectNumber;
            break;
               
            case 4:
    RunningNumber = RunningNumber + SelectNumber;
                break;
                
                default:
                break;
    }
    }
    Method = 1;
    SelectNumber = 0;
}

-(IBAction)Subtract:(id)sender
{
    if (RunningNumber == 0) {
        RunningNumber = SelectNumber;
    }
    else{
        
        switch(Method)
        {
            case 1:
                RunningNumber = RunningNumber * SelectNumber;
                break;
                
            case 2:
                RunningNumber = RunningNumber - SelectNumber;
                break;
                
            case 3:
                RunningNumber = RunningNumber / SelectNumber;
                break;
                
            case 4:
                RunningNumber = RunningNumber + SelectNumber;
                break;
                
            default:
                break;
        }
    }
    Method = 2;
    SelectNumber = 0;
   
}

-(IBAction)Devide:(id)sender
{
    if (RunningNumber == 0) {
        RunningNumber = SelectNumber;
    }
    else{
        
        switch(Method)
        {
            case 1:
                RunningNumber = RunningNumber * SelectNumber;
                break;
                
            case 2:
                RunningNumber = RunningNumber - SelectNumber;
                break;
                
            case 3:
                RunningNumber = RunningNumber / SelectNumber;
                break;
                
            case 4:
                RunningNumber = RunningNumber + SelectNumber;
                break;
                
            default:
                break;
        }
    }
    Method = 3;
    SelectNumber = 0;
    
  
    
}

-(IBAction)Plus:(id)sender
{
    if (RunningNumber == 0) {
        RunningNumber = SelectNumber;
    }
    else{
        
        switch(Method)
        {
            case 1:
                RunningNumber = RunningNumber * SelectNumber;
                break;
                
            case 2:
                RunningNumber = RunningNumber - SelectNumber;
                break;
                
            case 3:
                RunningNumber = RunningNumber / SelectNumber;
                break;
                
            case 4:
                RunningNumber = RunningNumber + SelectNumber;
                break;
                
            default:
                break;
        }
    }
    Method = 4;
    SelectNumber = 0;
}

-(IBAction)Equals:(id)sender
{
    if (RunningNumber == 0) {
        RunningNumber = SelectNumber;
    }
    else{
        
        switch(Method)
        {
            case 1:
                RunningNumber = RunningNumber * SelectNumber;
                break;
                
            case 2:
                RunningNumber = RunningNumber - SelectNumber;
                break;
                
            case 3:
                RunningNumber = RunningNumber / SelectNumber;
                break;
                
            case 4:
                RunningNumber = RunningNumber + SelectNumber;
                break;
                
            default:
                break;
        }
    }
    Method = 0;
    SelectNumber = 0;
    Screen.text = [NSString stringWithFormat: @"%2.f", RunningNumber];
    
}

-(IBAction)Clear:(id)sender
{
    Method = 0;
    RunningNumber = 0;
    SelectNumber = 0;
    
    Screen.text = [NSString stringWithFormat:@"0"];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
