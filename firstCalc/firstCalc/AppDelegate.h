//
//  AppDelegate.h
//  firstCalc
//
//  Created by Emad Oqdi on 15/05/2014.
//  Copyright (c) 2014 EmadOqdi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
