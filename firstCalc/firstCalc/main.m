//
//  main.m
//  firstCalc
//
//  Created by Emad Oqdi on 15/05/2014.
//  Copyright (c) 2014 EmadOqdi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
